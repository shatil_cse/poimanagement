package com.assignment.poimanagement.callback;

import com.assignment.poimanagement.utils.itemObjects.DescriptionItemObject;

/**
 * Created by shatil on 2/28/16.
 */
public interface DescriptionChangedListner {
    public void onDescriptionAdded(DescriptionItemObject item);
    public void onDescriptionRemoved(DescriptionItemObject item);
}
