package com.assignment.poimanagement.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by shatil on 2/27/16.
 * This is a singleton Sqlite db helper class
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "poi_database";
    public static final String POI_TABLE = "poi_table";
    public static final String DETAILS_TABLE = "description_table";
    private static final int DATABASE_VERSION = 1;
    public static final String POI_NAME = "poi_name", ID = "_id", TITLE = "title",
            DESCRIPTION = "description", POI_ID = "poi_id", PHOTO_URI = "photo_uri", DELETED = "deleted";
    public static DBHelper dbHelper;

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }

    public static DBHelper getInstances(Context context) {
        if (dbHelper == null) dbHelper = new DBHelper(context);
        return dbHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + POI_TABLE + " ( " + ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + POI_NAME + " TEXT NOT NULL,"
                + PHOTO_URI+ " TEXT,"
                + DELETED + " INTEGER DEFAULT 0);");

        db.execSQL("CREATE TABLE " + DETAILS_TABLE + " ( " + ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + POI_ID
                + " INTEGER NOT NULL, " + TITLE
                + " TEXT NOT NULL," + DESCRIPTION + " TEXT NOT NULL," + DELETED + " INTEGER DEFAULT 0);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
