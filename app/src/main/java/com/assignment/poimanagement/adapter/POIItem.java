package com.assignment.poimanagement.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.assignment.poimanagement.R;
import com.assignment.poimanagement.activity.POIDetailsActivity;
import com.assignment.poimanagement.db.DBHelper;
import com.assignment.poimanagement.utils.Log.CustomLogManager;

/**
 * Created by shatil on 2/28/16.
 */
public class POIItem extends CursorAdapter {
    Context context;

    public POIItem(Context context, Cursor c, int flag) {
        super(context, c, flag);
        this.context = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(
                R.layout.poi_item_adapter, parent,
                false);
        ViewHolder holder = new ViewHolder();
        holder.tvName = (TextView) view.findViewById(R.id.tv_name);
        holder.ivItemImage = (ImageView) view.findViewById(R.id.iv_item_photo);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        if (holder == null) {
            holder = new ViewHolder();
            holder.tvName = (TextView) view.findViewById(R.id.tv_name);
            holder.ivItemImage = (ImageView) view.findViewById(R.id.iv_item_photo);
            view.setTag(holder);
        }
        holder.tvName.setText(cursor.getString(cursor.getColumnIndex(DBHelper.POI_NAME)));

        String pathUri = cursor.getString(cursor.getColumnIndex(DBHelper.PHOTO_URI));
        if (pathUri != null && pathUri.length() > 0) {
            holder.ivItemImage.setImageURI(Uri.parse(pathUri));
        } else {
            holder.ivItemImage.setImageResource(R.drawable.fade_back);
        }

        final int poiId = cursor.getInt(cursor.getColumnIndex(DBHelper.ID));
        holder.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, POIDetailsActivity.class);
                intent.putExtra("poi_id", poiId);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                CustomLogManager.debugLog(getClass().getSimpleName(), "Clicked position: " + cursor.getPosition());
            }
        });
    }

    private static class ViewHolder {
        TextView tvName;
        ImageView ivItemImage;
    }
}
