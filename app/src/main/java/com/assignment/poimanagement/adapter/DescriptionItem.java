package com.assignment.poimanagement.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.assignment.poimanagement.R;
import com.assignment.poimanagement.utils.itemObjects.DescriptionItemObject;

import java.util.ArrayList;

/**
 * Created by shatil on 2/28/16.
 */
public class DescriptionItem extends RecyclerView.Adapter<DescriptionItem.ViewHolder>{

    ArrayList<DescriptionItemObject> dataItems;
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvTitle;
        public TextView tvDescription;

        public ViewHolder(View v) {
            super(v);
            tvTitle = (TextView) v.findViewById(R.id.tv_title);
            tvDescription = (TextView) v.findViewById(R.id.tv_description);
        }

    }

    /**
     *
     * @param position position of item
     * @param item single item object
     *             this will add a single item to the data set and notify changes
     */
    public void add(int position, DescriptionItemObject item) {
        dataItems.add(position, item);
        notifyItemInserted(position);
    }

    /**
     *
     * @param item single item object
     *             this will remove a single item from the data set and notify changes
     */
    public void remove(DescriptionItemObject item) {
        int position = dataItems.indexOf(item);
        dataItems.remove(position);
        notifyItemRemoved(position);
    }

    public DescriptionItem(ArrayList<DescriptionItemObject> dataItems) {
        this.dataItems=dataItems;
    }
    @Override
    public DescriptionItem.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.description_item_adapter, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //Showing up child views data
        final DescriptionItemObject item = dataItems.get(position);
        holder.tvTitle.setText(item.getTitle());
        holder.tvDescription.setText(item.getDescription());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dataItems.size();
    }

}