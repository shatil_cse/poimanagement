package com.assignment.poimanagement.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.assignment.poimanagement.R;
import com.assignment.poimanagement.utils.itemObjects.ImageItemDescription;
import com.assignment.poimanagement.utils.singletonUtils.VolleySingleton;

import java.util.ArrayList;

/**
 * Created by shatil on 11/28/16.
 */
public class NetworkImageItem extends RecyclerView.Adapter<NetworkImageItem.ViewHolder> {

    ArrayList<ImageItemDescription> dataItems = new ArrayList<>();
    ImageLoader mImageLoader;

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvTitle;
        public NetworkImageView tvImage;

        public ViewHolder(View v) {
            super(v);
            tvTitle = (TextView) v.findViewById(R.id.tv_title);
            tvImage = (NetworkImageView) v.findViewById(R.id.network_image_view);
        }

    }

    public void add(int position, ImageItemDescription item) {
        dataItems.add(position, item);
        notifyItemInserted(position);
    }

    public void reset(ArrayList<ImageItemDescription> items) {
        dataItems = items;
        Log.d("NetworkImageItem", "Items reseted size:" + items.size());
        notifyDataSetChanged();
    }

    /**
     * @param item single item object
     *             this will remove a single item from the data set and notify changes
     */
    public void remove(ImageItemDescription item) {
        int position = dataItems.indexOf(item);
        dataItems.remove(position);
        notifyItemRemoved(position);
    }

    public NetworkImageItem(Context context, ArrayList<ImageItemDescription> dataItems) {
        this.dataItems = dataItems;
        mImageLoader = VolleySingleton.getInstance(context).getImageLoader();
    }

    @Override
    public NetworkImageItem.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.description_item_network_image, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(NetworkImageItem.ViewHolder holder, int position) {
        final ImageItemDescription item = dataItems.get(position);
        holder.tvTitle.setText(item.getTitle());
        holder.tvImage.setImageUrl(item.getUrl(), mImageLoader);
    }

    @Override
    public int getItemCount() {
        return dataItems.size();
    }
}
