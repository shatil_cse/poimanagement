package com.assignment.poimanagement.activity;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.assignment.poimanagement.R;
import com.assignment.poimanagement.adapter.DescriptionItem;
import com.assignment.poimanagement.callback.DescriptionChangedListner;
import com.assignment.poimanagement.db.DBHelper;
import com.assignment.poimanagement.provider.POIListProvider;
import com.assignment.poimanagement.utils.Log.CustomLogManager;
import com.assignment.poimanagement.utils.itemObjects.DescriptionItemObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by shatil on 2/28/16.
 */
public class POIEditActivity extends AppCompatActivity implements DescriptionChangedListner, View.OnClickListener {

    Context context;
    Bitmap bitmap = null;
    long poiId; //this is the id of poi table
    EditText etName;
    EditText etTitle;
    EditText etDescription;
    ImageView ivPhoto;
    DescriptionItem adapter;
    TextView tvAddDescription;
    RecyclerView rvDescription;
    ArrayList<DescriptionItemObject> dataItems = new ArrayList<DescriptionItemObject>();
    String photoUri = null, name;
    SQLiteDatabase db;
    String filepath = "POIImageFolder";
    int SELECT_GALARY_PHOTO = 101, SELECT_CAMERA_PHOTO = 201;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_details);
        context = getApplicationContext();
        db = DBHelper.getInstances(context).getWritableDatabase();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //get poi id from intent if it is -1 then it's an add poi operation
        poiId = getIntent().getIntExtra("poi_id", -1);

        if(poiId>0)
        {
            //it's a update operation so load the name and photo uri first
            loadData();
        }
        //set title according to operation
        if (poiId == -1) {
            getSupportActionBar().setTitle("Add New");
        } else {
            getSupportActionBar().setTitle("Edit");
            dataItems = loadAdapterData(poiId);
        }
        setUI();
    }

    /**
     * find all view and sets initial UI
     */
    private void setUI()
    {
        ivPhoto = (ImageView) findViewById(R.id.iv_image);
        rvDescription = (RecyclerView) findViewById(R.id.rv_description);
        etName = (EditText) findViewById(R.id.et_name);
        etTitle = (EditText) findViewById(R.id.et_title);
        etDescription = (EditText) findViewById(R.id.et_description);
        tvAddDescription = (TextView) findViewById(R.id.tv_add_description);
        tvAddDescription.setOnClickListener(this);
        rvDescription = (RecyclerView) findViewById(R.id.rv_description);
        rvDescription.setHasFixedSize(true);
        rvDescription.setLayoutManager(new LinearLayoutManager(this));
        adapter = new DescriptionItem(dataItems);
        rvDescription.setAdapter(adapter);
        etName.setText(name);
        if (photoUri != null) {
            ivPhoto.setImageURI(Uri.parse(photoUri));
        }
    }
    @Override
    public void onBackPressed() {
        if (etName.getText().toString() != null && etName.getText().toString().trim().length() > 0) {
            //Valid name given so insert or update it before finishing add/edit
            insertOrUpdatePOI(etName.getText().toString(), photoUri);
            super.onBackPressed();
        } else if (poiId == -1) {
            //It's a new insert but no POI name has given so don't insert it
            super.onBackPressed();
        } else {
            //An existing POI must have a NAme so restrict user from going back with out a Name
            etName.setError("Enter a Name");
            Toast.makeText(context, "Please enter a name", Toast.LENGTH_LONG).show();
        }

    }

    /**
     *
     * @param poiId id of POI
     * @return ArrayList of DescriptionItemObject to set list of description
     * It loads data from description table and make an ArrayList
     */
    private ArrayList<DescriptionItemObject> loadAdapterData(long poiId) {
        ArrayList<DescriptionItemObject> dataList = new ArrayList<DescriptionItemObject>();
        Cursor cursor = db.query(DBHelper.DETAILS_TABLE, null, DBHelper.POI_ID + " =? AND " + DBHelper.DELETED + " =? ",
                new String[]{String.valueOf(poiId), String.valueOf(0)}, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                dataList.add(new DescriptionItemObject(cursor));
                cursor.moveToNext();
            }
        }
        if (cursor != null) cursor.close();
        return dataList;

    }

    /**
     * loads photo uri and name for passed poi id
     */
    private void loadData() {
        Cursor cursor = db.query(DBHelper.POI_TABLE, null, DBHelper.ID + " =? AND " + DBHelper.DELETED + " =? ",
                new String[]{String.valueOf(poiId), String.valueOf(0)}, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            name = cursor.getString(cursor.getColumnIndex(DBHelper.POI_NAME));
            photoUri = cursor.getString(cursor.getColumnIndex(DBHelper.PHOTO_URI));
        }
        if (cursor != null) cursor.close();
    }

    /**
     *
     * @param name Valid Name of POI
     * @param photoUri photo uri of POI
     * @return
     */
    private long insertOrUpdatePOI(String name, String photoUri) {

        ContentValues cv = new ContentValues();
        cv.put(DBHelper.PHOTO_URI, photoUri);
        cv.put(DBHelper.POI_NAME, name);
        if (poiId == -1) {
            //if there it is not inserted yet then insert it and return newly inserted item id
            Uri uri;
            uri = getContentResolver().insert(POIListProvider.CONTENT_URI, cv);
            return ContentUris.parseId(uri);
        } else {
            //if already has an entry then update it and return existing poiId
            getContentResolver().update(POIListProvider.CONTENT_URI, cv,
                    DBHelper.ID + " =? ", new String[]{String.valueOf(poiId)});
            return poiId;
        }

    }

    /**
     *
     * @param poiId id of POI
     * @param title title of description
     * @param description description
     * @return newly inserted rowId;
     */
    private long insertDetails(long poiId, String title, String description) {
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.POI_ID, poiId);
        cv.put(DBHelper.TITLE, title);
        cv.put(DBHelper.DESCRIPTION, description);
        return db.insert(DBHelper.DETAILS_TABLE, null, cv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_open_camera:
                opencamera();
                break;
            case R.id.action_open_galary:
                openGalary();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_add_description:
                setNewDescriptionIfPossible();
                break;
        }
    }

    private void setNewDescriptionIfPossible() {
        try {

            if (poiId < 0) {
                //This is a new entry so try to insert it if there is any name for POI has entered
                if (etName.getText().toString().length() == 0) {

                    Toast.makeText(context, "Plaese enter a name first", Toast.LENGTH_LONG).show();
                    return;
                }
                poiId = insertOrUpdatePOI(etName.getText().toString(), photoUri);
            }
            //Now try to validate description data and then insert the valid data otherwise show message
            String description = etDescription.getText().toString();
            String title = etTitle.getText().toString();
            if (description.length() == 0 || title.length() == 0) {
                Toast.makeText(context, "Plaese check Title and Description", Toast.LENGTH_LONG).show();
            } else {
                long insertedRowId = 0;
                //description and title exists so insert new description
                insertedRowId = insertDetails(poiId, title, description);
                if (insertedRowId > 0) {
                    //description inserted successful so triger the callback listener
                    // to update UI and clean edittext fields
                    Log.d(getClass().getSimpleName(), "Successfully entered");
                    Cursor cursor = db.query(DBHelper.DETAILS_TABLE, null,
                            DBHelper.ID+" =? AND " + DBHelper.DELETED + " =? ",
                            new String[]{String.valueOf(insertedRowId), String.valueOf(0)}, null, null, null);
                    if (cursor != null && cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        this.onDescriptionAdded(new DescriptionItemObject(cursor));
                        etTitle.setText("");
                        etDescription.setText("");
                    }
                    if (cursor != null) cursor.close();
                }
            }
        } catch (Exception e) {
            CustomLogManager.debugLog(getClass().getSimpleName(),e.toString());
        }
    }

    @Override
    public void onDescriptionAdded(DescriptionItemObject item) {
        //Add new items to adapter
        adapter.add(adapter.getItemCount(), item);
    }

    @Override
    public void onDescriptionRemoved(DescriptionItemObject item) {
        //remove deleted item from adapter
        adapter.remove(item);
    }

    public String saveBitmap(Bitmap bitmap) {
        String filename = "image" + String.valueOf(System.currentTimeMillis());
        ContextWrapper contextWrapper = new ContextWrapper(
                getApplicationContext());
        File directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);
        final File imagefile = new File(directory, filename);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagefile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
            return imagefile.getAbsolutePath();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void openGalary() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_GALARY_PHOTO);
    }

    private void opencamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, SELECT_CAMERA_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_CAMERA_PHOTO) {
                //camera...
                Bundle extras = data.getExtras();
                bitmap = (Bitmap) extras.get("data");
            } else if (requestCode == SELECT_GALARY_PHOTO) {
                try {
                    Uri selectedImage = data.getData();
                    InputStream imageStream = getContentResolver().openInputStream(selectedImage);
                    bitmap = BitmapFactory.decodeStream(imageStream);
                } catch (Exception e) {
                    return;
                }
            }
            if (bitmap != null) {
                new SaveBitmap().execute();
            }

        }
    }

    private class SaveBitmap extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            return saveBitmap(bitmap);
        }

        @Override
        protected void onPostExecute(String path) {
            photoUri = path;
            ivPhoto.setImageURI(Uri.parse(photoUri));
            super.onPostExecute(path);
        }
    }
}
