package com.assignment.poimanagement.activity;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.assignment.poimanagement.R;
import com.assignment.poimanagement.adapter.POIItem;
import com.assignment.poimanagement.db.DBHelper;
import com.assignment.poimanagement.provider.POIListProvider;

public class ActivityPOIList extends AppCompatActivity {

    int POI_LOADER_ID = 1;
    Context context;
    POIItem adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poilist);
        context = getApplicationContext();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("POI Managements");
        adapter = new POIItem(context, null, 0);
        ListView lvName = (ListView) findViewById(R.id.lv_poi);
        lvName.setAdapter(adapter);
        FloatingActionButton fabAddNewPOI = (FloatingActionButton) findViewById(R.id.fab_add_new);
        //As there is only one onClickListener so implement it just here
        fabAddNewPOI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, POIEditActivity.class);
                intent.putExtra("poi_id", -1);
                //startActivity(intent);
                startActivity(new Intent(context, ShowListOfNetworkImage.class));
            }
        });
        //start the cursor loader
        getLoaderManager().restartLoader(POI_LOADER_ID, null, new POItListLoaderCallback());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_poilist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_add:
                Intent intent = new Intent(context, POIEditActivity.class);
                intent.putExtra("poi_id", -1);
                startActivity(intent);
                break;
            case R.id.action_send:
                sendEmail();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendEmail() {
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent.setType("vnd.android.cursor.item/email");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"shatil200@gmail.com"});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Assignment Review");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(emailIntent, "Send email.."));
    }

    /**
     * POIListLoaderCallBack for cursor loader of POIList
     */
    private class POItListLoaderCallback implements LoaderManager.LoaderCallbacks<Cursor> {

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            CursorLoader cursorLoader = null;
            if (POI_LOADER_ID == id) {
                //get cursorloader using Content Provider poi_table uri
                //so that when data of poi_table gets changed it gets call automatically
                cursorLoader = new CursorLoader(ActivityPOIList.this,
                        POIListProvider.CONTENT_URI,
                        null,
                        DBHelper.DELETED + " = ? ",
                        new String[]{String.valueOf(0)},
                        DBHelper.ID + " DESC");
            }
            return cursorLoader;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            adapter.swapCursor(data);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            adapter.swapCursor(null);
        }
    }
}
