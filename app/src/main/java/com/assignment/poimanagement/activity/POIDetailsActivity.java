package com.assignment.poimanagement.activity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.assignment.poimanagement.R;
import com.assignment.poimanagement.adapter.DescriptionItem;
import com.assignment.poimanagement.db.DBHelper;
import com.assignment.poimanagement.utils.itemObjects.DescriptionItemObject;

import java.util.ArrayList;

/**
 * Created by shatil on 2/28/16.
 */
public class POIDetailsActivity extends AppCompatActivity {

    Context context;
    int poiId;
    ImageView ivPhoto;
    String name;
    DescriptionItem adapter;
    RecyclerView rvDescription;
    ArrayList<DescriptionItemObject> dataItems;
    String photoUri = null;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_details);
        context = getApplicationContext();
        db = DBHelper.getInstances(context).getWritableDatabase();
        //get id from intent
        poiId = getIntent().getIntExtra("poi_id", -1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //refresh Data and UI on onResume because there might be some
        // data changes while the activity was in onPause
        refreshDataAndUI();

    }

    /**
     * load data from id and refresh UI
     */
    private void refreshDataAndUI()
    {
        //load name and photo uri
        loadData();
        //refresh UI
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Contact Details");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        CollapsingToolbarLayout collapsingToolbarLayout=(CollapsingToolbarLayout) findViewById(R.id.collapsing_tool_bar);
        collapsingToolbarLayout.setTitle(name);
        ivPhoto=(ImageView)findViewById(R.id.iv_image);
        rvDescription=(RecyclerView) findViewById(R.id.rv_description);
        rvDescription = (RecyclerView) findViewById(R.id.rv_description);
        rvDescription.setHasFixedSize(true);
        rvDescription.setLayoutManager(new LinearLayoutManager(this));
        dataItems = loadAdapterData(poiId);
        adapter = new DescriptionItem(dataItems);
        rvDescription.setAdapter(adapter);
        if (photoUri != null) {
            ivPhoto.setImageURI(Uri.parse(photoUri));
        }
    }

    /**
     *
     * @param poiId id of POI
     * @return ArrayList of DescriptionItemObject to set list of description
     * It loads data from description table and make an ArrayList
     */
    private ArrayList<DescriptionItemObject> loadAdapterData(int poiId) {
        ArrayList<DescriptionItemObject> dataList = new ArrayList<DescriptionItemObject>();
        Cursor cursor = db.query(DBHelper.DETAILS_TABLE, null, DBHelper.POI_ID + " =? AND " + DBHelper.DELETED + " =? ",
                new String[]{String.valueOf(poiId), String.valueOf(0)}, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                dataList.add(new DescriptionItemObject(cursor));
                cursor.moveToNext();
            }
        }
        if (cursor != null) cursor.close();
        return dataList;

    }

    /**
     * load name and photo uri using id
     */
    private void loadData() {

        Cursor cursor = db.query(DBHelper.POI_TABLE, null, DBHelper.ID+" =? AND "+DBHelper.DELETED + " =? ",
                new String[]{String.valueOf(poiId),String.valueOf(0)}, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            name=cursor.getString(cursor.getColumnIndex(DBHelper.POI_NAME));
            photoUri = cursor.getString(cursor.getColumnIndex(DBHelper.PHOTO_URI));
        }
        if (cursor != null) cursor.close();
    }

    /**
     *
     * @param poiId id to delete
     * It basically makes the delete flag true
     */
    private void deletePOI(long poiId)
    {
        ContentValues cv=new ContentValues();
        cv.put(DBHelper.DELETED,1);
        db.update(DBHelper.POI_TABLE,cv,DBHelper.ID+" =? ",new String[]{String.valueOf(poiId)});
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_edit:
                Intent intent = new Intent(context, POIEditActivity.class);
                intent.putExtra("poi_id", poiId);
                startActivity(intent);
                break;
            case R.id.action_delete:
                deletePOI(poiId);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_details, menu);
        return true;
    }


}
