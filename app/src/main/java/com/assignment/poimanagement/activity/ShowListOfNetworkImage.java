package com.assignment.poimanagement.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.assignment.poimanagement.R;
import com.assignment.poimanagement.adapter.NetworkImageItem;
import com.assignment.poimanagement.utils.itemObjects.ImageItemDescription;
import com.assignment.poimanagement.utils.singletonUtils.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by shatil on 11/28/16.
 */
public class ShowListOfNetworkImage extends AppCompatActivity implements Response.Listener<JSONArray>, Response.ErrorListener {
    Context context;
    NetworkImageItem adapter;
    RecyclerView rvDescription;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_network_images);
        context = getApplicationContext();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Super hit movies");
        rvDescription = (RecyclerView) findViewById(R.id.rv_network_images);
        rvDescription.setHasFixedSize(true);
        rvDescription.setLayoutManager(new LinearLayoutManager(this));
        adapter = new NetworkImageItem(context, new ArrayList<ImageItemDescription>());
        String url = "http://api.androidhive.info/json/glide.json";
        JsonArrayRequest jsObjRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, this, this);
        VolleySingleton.getInstance(this.getApplicationContext()).
                addToRequestQueue(jsObjRequest);
        progressDialog = ProgressDialog.show(this, "", "Loading Image urls");
    }

    private ArrayList<ImageItemDescription> getDataItems(JSONArray jsonArray) {
        ArrayList<ImageItemDescription> dataItems = new ArrayList<>();
        try {
            for (int position = 0; position < jsonArray.length(); position++) {
                JSONObject object = jsonArray.getJSONObject(position);
                JSONObject urls = object.getJSONObject("url");
                dataItems.add(new ImageItemDescription(object.getString("name"), urls.getString("medium")));
            }
        } catch (Exception e) {
            Log.d("ShowListofNetWorkImage", "ParsingError:" + e.toString());
        }
        return dataItems;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d("ShowListofNetWorkImage", error.getMessage());
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onResponse(JSONArray response) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        //adapter.reset(getDataItems(response));
        adapter = new NetworkImageItem(context, getDataItems(response));
        rvDescription.setAdapter(adapter);
        Log.d("ShowListofNetWorkImage", "Response:\n" + response.toString());
    }
}
