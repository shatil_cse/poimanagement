package com.assignment.poimanagement.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.assignment.poimanagement.db.DBHelper;

import java.sql.SQLException;

/**
 * Created by shatil on 3/4/16.
 */
public class POIListProvider extends ContentProvider {
    private SQLiteDatabase db;
    static final String PROVIDER_NAME = "com.assignment.poimanagement.provider";
    static final String path = "poilists";
    static final String URL = "content://" + PROVIDER_NAME + "/" + path;
    public static final Uri CONTENT_URI = Uri.parse(URL);
    static final int POI_LIST = 1;
    private UriMatcher uriMatcher;

    @Override
    public boolean onCreate() {
        db = DBHelper.getInstances(getContext()).getWritableDatabase();
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, path, POI_LIST);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor = null;
        switch (uriMatcher.match(uri)) {
            case POI_LIST:
                cursor = db.query(DBHelper.POI_TABLE, projection, selection, selectionArgs, null, null, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(), uri);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        String contentType = null;
        switch (uriMatcher.match(uri)) {
            case POI_LIST:
                contentType = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.assignment.poilistprovider";
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return contentType;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri itemUri = null;
        switch (uriMatcher.match(uri)) {
            case POI_LIST:
                long rowID = db.insertOrThrow(DBHelper.POI_TABLE, null, values);
                itemUri = ContentUris.withAppendedId(CONTENT_URI, rowID);
                getContext().getContentResolver().notifyChange(itemUri, null);
                break;

            default:
                throw new UnsupportedOperationException("Insert not supported on URI: " + uri);
        }
        return itemUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case POI_LIST:
                count = db.delete(DBHelper.POI_TABLE, selection, selectionArgs);
                getContext().getContentResolver().notifyChange(uri, null);
                break;

            default:
                throw new UnsupportedOperationException("Delete not supported on URI: " + uri);
        }
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case POI_LIST:
                count = db.update(DBHelper.POI_TABLE, values, selection, selectionArgs);
                getContext().getContentResolver().notifyChange(uri, null);
                break;

            default:
                throw new UnsupportedOperationException("Update not supported on URI: " + uri);
        }
        return count;
    }
}
