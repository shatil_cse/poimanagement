package com.assignment.poimanagement.utils.Log;

import android.util.Log;

/**
 * Created by shatil on 3/4/16.
 * This class will centralize the log message for whole app
 * This will make debuging easier
 */
public class CustomLogManager {
    public static void debugLog(String className,String log)
    {
        Log.d("POILogManager",className+" --> "+log);
    }
}
