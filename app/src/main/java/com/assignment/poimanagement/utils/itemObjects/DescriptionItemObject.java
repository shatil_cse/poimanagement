package com.assignment.poimanagement.utils.itemObjects;

import android.database.Cursor;

import com.assignment.poimanagement.adapter.DescriptionItem;
import com.assignment.poimanagement.db.DBHelper;

/**
 * Created by shatil on 2/28/16.
 */
public class DescriptionItemObject {

    //private Cursor cursor;
    private String title;
    private String description;
    public DescriptionItemObject(Cursor cursor)
    {
        //this.cursor=cursor;
        title=cursor.getString(cursor.getColumnIndex(DBHelper.TITLE));
        description=cursor.getString(cursor.getColumnIndex(DBHelper.DESCRIPTION));
    }
    public String getDescription()
    {
        return description;
    }
    public String getTitle()
    {
        return title;
    }
}
