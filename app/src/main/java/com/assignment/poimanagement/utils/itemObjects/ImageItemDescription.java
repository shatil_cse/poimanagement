package com.assignment.poimanagement.utils.itemObjects;

/**
 * Created by shatil on 11/28/16.
 */
public class ImageItemDescription {
    private String title;
    private String url;

    public ImageItemDescription(String title, String url) {
        this.title = title;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }
}
