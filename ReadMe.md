Getting Started
===============
This is a very simple demo app which contains three activities Point Of Interest(POI) list,details of POI,
add/edit an existiing POI.
This simple project contains very simple implementation of SQLiteDBHelper class, singleton class, content provider,Cursor Loader, Cursor Adapter,RecyclerView, CardView, Material Design Scrolling effect.

Start Using App
===============
First page contains Names and images of POI, menubar gives an option to add a new POI,Details page menubar gives option to edit or delete a POI.
From add/edit page one can add/edit a POI and from menubar one can change image of POI.

Goal of this demo App:
======================
User can add or remove a POI and image,some description related to that POI.